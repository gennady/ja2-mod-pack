Guerilla Warfare by Romualdas Arm:


This pak was originally designed to work with the Wildfire 3.0 patch for Jagged Alliance 2. You must have the Wildfire patch installed correctly before using the W4st editor to make changes in item table.  If you going to use this Pak without  Wildfire Mod you should not use Items table!  In case no Wildfire mod is installed you can get a lot of problems with guns & ammo of your mercenaries,  the weapons the merks bring from AIM could not match the ammo they bring with, include wrong cartriges types & different number of rounds in cartriges & weapons they made for. So I'm strongly recommend do not use this product alone.

Pak now exist in some variations: Basic (simple) - first release version,   Improved - corrected after remarks of  experienced players & Expert version which continues the line started in improved version.
The item table is same for all version & supported in two variations, the only difference is first  wgfire.zip  containing V-94  & second wrr.zip original  Rocket Rifle parameters & description.

Basic version
This version consists of two archives:
First one, prime.zip contain maps of Drassen, Kambria, Chitsena, Gramm, Alma, Balime, Orta,"Down Jail" & SAMs
Second , country.zip country maps

Diffeernce between  Basic version & JA2 original maps
- Added a lot of grass & bushes- natural growth  like it is in real life in Latin America, so battle succes depend of hiding abilyty & experience, but not only of number & equipment;
- Guarded installation  made more defended by changing illumination, making large cleared space around it &  adding  of mine fields with high trap level (5-9) mines;
- Scripts of enemy soldiers  changed to   make them more sutable to defending;
- Garrison soldiers are equipped with night ops equipment, they got more improved weapons & are armored better. Their placement changed to make them cover each other;
- There are some new roof snipers now;
- Garrison in Alma equipped with not dropable UV goggles;
- Garrisson in Balime strenghtened bu GreyShirts;
- Number of ammo in Bobby ray stock increased,  he also sells chemical flares now & not sell pistols & sniper rifles anymore.
- All Explosives damage increased by ten

Improved version
In this version I changed the way of sorting maps & made it more shared, so there are single archives for each town & one more with SAM sites & "Down Jail".
Drassen, Cambria, Chitsena, Grumm, Alma, Balime, Orta,"Down Jail" & SAMs
The difference between basic & improved version mostly affect enemy soldiers parameters & equipment options.
Improved verson was made due complaints of experienced players that after installing pack game is still too easy, so I improved all the enemy soldiers in  all town sectors.

Changes were made in Impoved version:
- there are no more RND generated characteristics for enemy soldiers now. All of them have exact experience level no lower the 5 (up to 8), elites no lower the 7(up to 9), & health no lower the 80 (up to 99) the lowest marksmanship of enemy now is 75.
- there is no armourless soldiers in towns squares anymore. Flack jackets are on soldiers  in Omerta & Drassen only,  in other towns    armour of soldiers is more impoved & it differs from squre to square in single town, depending on this square  importance up to compaunded spectra with ceramic plates.
- there is no more "blind guardians" - all soldiers have night vision equipmen: Night vision goggles in Drassen,  in  Chitsena mostly too, but there some UV goggle already,  in Cambria the share of UV equipment is near 50%, in Grum much more,  in SAM sites & Alima the soldiers are 100% UV equipped
- Enemy soldiers are equipped with chemical flares, so if you are not carefull enough you will be spotted in lights...
- Enemy soldiers have presetted weapons, starting is MP5A3 & Commando & so on - the more important sector is, the more improved weapons they have.

And at last the main radical changes offered by Serg Wildfire:
- no good weapons  will drop from dead enemyes in town sectors except critical hits anymore. One single exception was made in  Drassen, there   left  one dropable SKS in airstrip square. In all other  town sector, no matter of town you are in, no weapons will left after enemy death.  In country sectors weapons are still dropable within usual probability, so if you not going to wait visitors, you are to get some weapons from enemy patros, & you  still  can   try to stole one from live enemy, but I don't like such way so it's your own choice...
- no night vision or UV goggles will drop from dead enemy in town sectors anymore, so in night you will to fight armed with your own eyes only, if only not get one from enemy patrols. It' means all dropable night vision equipment you used to find in SAM sites or in sectors of  Alma are gone... No panic - Bobby will help you - at the starting stage with chemical flares & according the  progress with night goggles too... :)
Tiksa & Meduna was not changed yet.

Expert version
In fact there only  I can say about this version: It's next step of improved version - cause there are still used original maps of Ja2 for country sectors with some not so little modifications, but in the same time it's not cause there is some new maps that have no relations with original sir-tech at all.
This section maps, like all maps in other sections are optional gear. If you fill they are too hard for you, then you are free to uninstall them.
This section was born due lot number of wishes to see Meduna in line with all other towns -  players complained  that after they passed modified towns they fill themselves in Meduna too freely.
Here is the answer:
1)  I'm to represent you Meduna's defencive ring consists of 6 maps: M2; M3; M4; M5; M6 & N6
The maps are experimental, in they organisation used some different ways to construct defencive perimeter, but they are tested already & they all were captured. But main testers remark is - "there is no way to pass this maps with no casualties playing first time. The casualty rating is from 60 to 100%"
The maps were organised to turn  your life in to hell: you have very small choise of inserting location; there is a lot of mortar nests, no less the two on each map; there is mostly no chances  to insert unspotted; maneuvering to atack enemy from back some time could be more dangerous them frontal attack; mines mines & mines again & so on - lot of surprises...
2) And two new sector of Meduna
Arulco Internationale (Airfield)
Minesterio de Fuerzas (Military ministry = SAM site)
The only I can say about this two - they look peacefully... ;)
3) Here we got some more sectors, made under motto - AMBUSH!
beta country
4) Here is  prof.dat with new mercenary data  just place it to ja2\data\binarydata\ 
________________________________________
Installation
    To install pak you are to copy  *.dat files to ja2\data\maps, if such folder does not exist you are to create it. 
    To make  containing in  WfireG.txt use ja2wedit, available from  http://www.strategyplanet.com/jaggedalliance/   in Ja2 Download section or other way- ask W4st, using standart procedure described in ja2wedit manual.
Uninstall
    To uninstall Pak just delete maps from ja2\data\maps folder     To  undo the changes containing in  WfireG.txt use ja2wedit, available from  http://www.strategyplanet.com/jaggedalliance/   in Ja2 Download section or other way- ask W4st, using standart procedure described in ja2wedit manual.
 
Yours faithfully, Romualdas Arm Romualdas_arm@yahoo.com
________________________________________
Some notes:
There are a lot of things I was not able to do, cause I'm not a programmer...
The table of items just moves wildfire 3.0 near 3.16 rus there are lot of things it missed ...
I'm not able to change number or garrison troops &  any  counter & any attack conditions cause it seem  Ja2 override map settings by it's own 
The Metall detector is the great joke cause modern mines can't be found by it - the only way to find them is using probe.
The helicopter is the cheating vehicle to my mind & i'm not using it.
Truly yours, Romualas Arm

P.S:Please, report all bugs you find to:  romualdas_arm@yahoo.com