# JA2 Mod Pack

This is a compilation of various modifications to Jagged Alliance 2 game.
The project is in its very early stage.

**Important note**: every mod included into this pack has its owner.  If you are
the owner of one of the included mods and don't like your mod being
distributed this way, please contact the project maintainer, so the issue can
be resolved promptly.

List of mods included into the pack:

- *alter-ego-voices* - three Russian voices for alter ego characters
- *from-russia-with-love* - a tiny demo mod
- *nightops-maps* - maps from Night Ops mod
- *wf3-guerilla-warfare-basic* - basic maps from WF3 Guerilla Warfare mod

## How to start playing

### Install original Jagged Alliance 2

First of all, you will need a copy of the original Jagged Alliance 2 game.
Install it as usual.

### Setup JA2-Stracciatella

This mod pack uses JA2-Stracciatella's executable.  It is included into the pack.

JA2-Stracciatella can work with many versions of Jagged Alliance 2 data files.
You should start the game by running one of the bat files, corresponding your
version of the game files.

For example, if you have English version installed, start the game by running
ja2-ENGLISH.bat.  If you have French version installed, start the game by
running ja2-FRENCH.bat, etc.

The first time you start the game, a directory "JA2" is created in your user
directory ("My documents").  In there is a file "ja2.ini".  Edit this file and
insert the correct path to the Jagged Alliance 2 data files in the line
"data_dir = [...]".  If $DIR is the directory, then it shall contain:

- $DIR/Data/*.slf           - all .slf files from the installation AND game CD
- $DIR/Data/TILECACHE/*.sti - all files found in the TILECACHE directory from
- $DIR/Data/TILECACHE/*.jsd   the installed game

It is possible to change game resolution using command-line key '-res'.
For example, to start the game in 1024x768 mode, launch ja2.exe like this:

  ja2.exe -res 1024x768

Any reasonable combination of width and height should be possible.  You can
experiment and find one that suits you best.

### Start the mod

If you are using English version of the game, execute one of the files:

```
ja2-mod-*.bat
```

If you are not using the English version, you will need to modify
ja2-mod-*.bat files by adding parameter -resversion VERSION_NAME
(e.g. -resversion GERMAN).  Execute ja2.exe -help for options.

## License

License of JA2 Stracciatella is complicated.
See [JA2 Stracciatella project](https://github.com/gennady/ja2-stracciatella) for more details.

Every mod has its onw license.  See file license.txt in mod's directory.
